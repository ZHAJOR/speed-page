# Speed page

The goal of this project is to get the average time of loadind of a page.
To do this i made two simple scripts, one using `wget` and the other `curl`.
As wget can't return the time, `time` is used.

### Auto-install

Just download the project, and then run the `install` file.

### Start

You need :
* wget and time
* curl

Then, just download the sources, if they don't have the execution rights do :

```shell
chmod +x speedcurl
chmod +x speedwget
```

If you want to use them from anywhere (then don't use `./`):
```shell
move speedcurl /usr/local/bin
move speedwget /usr/local/bin
```

### Parameters and results

They take the same parameters, first the name of the website, then the nombre of requests.

The result 'Average' is the average time that a request take (milliseconds).
The result 'Total' is the total time for all the resquest asked (seconds).
The result 'Requests' is the number of requests asked.

### Execute speedcurl

```shell
./speedcurl google.fr 10
```

You will get :

```shell
Average : 476.60000 ms
Total: 4.766 s
Requests: 10
```

### Execute speedwget
```shell
./speedwget google.fr 10
```

You will get :

```shell
Average : 536.00000 ms
Total: 5.36 s
Requests: 10
```

### Others

I saw that speedcurl is often faster that speedwget.
I tried with 2000 requests and get an average 7ms quicker with curl (google.fr).
The option -L for curl is activated to be able to make redirections, but to be sure feel free to precise https://www.site.com
